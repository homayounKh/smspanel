const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        redirect: {name: 'home'}
      },

      {
        path: 'home',
        name: 'home',
        component: () => import('pages/Home.vue')
      },

      {
        path: 'setting',
        name: 'setting',
        component: () => import('pages/Setting.vue')
      },

      {
        path: 'send-sms',
        name: 'sendSms',
        component: () => import('pages/SendSms.vue')
      },

      {
        path: 'tracking',
        name: 'tracking',
        component: () => import('pages/Tracking.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
