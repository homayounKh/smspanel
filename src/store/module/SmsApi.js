import Utils from "src/js/Utils";
import Kavenegar from "kavenegar";
import Cookie from "src/js/Cookie";
import {CK_KAVENEGAR_API} from "src/js/Consts";

export const ACT_SMSAPI = {
  SEND_SMS: 'SEND_SMS',
  RECEIVE_SMS: 'RECEIVE_SMS',
  CHECK_STATUS_SMS: 'CHECK_STATUS_SMS',
  GET_SMS_PANEL_INFO: 'GET_SMS_PANEL_INFO',
}

export default {
  actions: {
    [ACT_SMSAPI.SEND_SMS](_, {message='', sender='', receptor=''}) {
      return new Promise((resolve, reject) => {

        Utils.sendRequest({
          url: `https://api.kavenegar.com/v1/${Cookie.get(CK_KAVENEGAR_API)}/sms/send.json`,
          param: {
            "message": message,
            "sender": sender,
            "receptor": receptor,
          },
          method: 'get'
        })
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    [ACT_SMSAPI.RECEIVE_SMS](_, {startDate, endDate}) {
      return new Promise((resolve, reject) => {

        Utils.sendRequest({
          url: `https://api.kavenegar.com/v1/${Cookie.get(CK_KAVENEGAR_API)}/sms/selectoutbox.json`,
          param: {
            "startdate": startDate,
          },
          method: 'get'
        })
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
      })
    },


    [ACT_SMSAPI.CHECK_STATUS_SMS](_, {messageId}) {
      return new Promise((resolve, reject) => {

        Utils.sendRequest({
          url: `https://api.kavenegar.com/v1/${Cookie.get(CK_KAVENEGAR_API)}/sms/status.json`,
          param: {
            "messageid": messageId,
          },
          method: 'get'
        })
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
      })
    },


    [ACT_SMSAPI.GET_SMS_PANEL_INFO]() {
      return new Promise((resolve, reject) => {

        Utils.sendRequest({
          url: `https://api.kavenegar.com/v1/${Cookie.get(CK_KAVENEGAR_API)}/account/info.json`,
          method: 'get'
        })
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  },
}
