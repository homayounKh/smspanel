import axios from "axios";
import {ConvertTo} from "./PersianTools";

function sendRequest({
                   url = "", method = "", filter = {},
                   param = {}, dataObj = {},
                   header = {}, limit = 0, index = 0,
                 }) {


  // meta = {
  //     "all": 0,
  // }

  const ax = axios.create({
    headers: header,
    timeout: ''
  });

  ax.interceptors.request.use((con) => {
    // console.log(con.data);
    return con;
  })
  ax.interceptors.response.use((con) => {
    // console.log(con.data);
    return con;
  }, error => {
    if (error.response) {

    } else {
      return Promise.reject(error);
    }
  })


  return new Promise((resolve, reject) => {
    ax.request({
      url: url,
      method: method,
      params: param,
      data: dataObj,

    }).then((response) => {
      resolve(response);

    }).catch((error) => {

      reject(error);
    })
  });
}

// is valid national code
function isValidNC(nc) {
  var sum = 0, i, zarib = 10;

  var n = nc.length;

  // declaring character array
  var national_Code = [];

  if (nc == "1234567890" || nc == "9876543210")
    return false;
  if (nc.length == 10) {

    national_Code = nc.split("");

    var same_check = 0;
    for (let ii = 1; ii < 11; ii++) {
      if (national_Code[ii] == national_Code[ii - 1]) {
        same_check++;
      }
    }

    if (same_check == 9) {
      return false;
    }
    for (i = 0; i < 9; i++, zarib--) {

      sum = sum + (parseInt(national_Code[i]) - parseInt('0')) * zarib;
    }
    sum = sum % 11;
    if (sum < 2) {

      return sum == (parseInt(national_Code[9]) - parseInt('0'));
    } else {
      return (11 - sum) == (parseInt(national_Code[9]) - parseInt('0'));
    }
  } else
    return false;
}


export const UtilsMethods = {
  methods: {
    getDayDate(isToday = true, numMonth = 0) {

      // numMonth : several months ago (signed number) - several months later (unsigned number)

      var grDate;
      const date = new Date();

      if (isToday) {
        grDate =
          date.getFullYear() + "-" +
          (date.getUTCMonth() + 1) + "-" +
          date.getDate();
      } else {
        date.setMonth(date.getMonth() + numMonth)
        grDate =
          date.getFullYear() + "-" +
          (date.getUTCMonth() + 1) + "-" +
          date.getDate();
      }
      return ConvertTo.methods.toJalaliStr(grDate);
    },
    numberFilter(value) {
      return numberFilter(value);
    },
    getObjectByID(list, id, idKey) {
      const li = list.filter((obj, index) => {
        return obj[idKey ? idKey : 'ID'] == id;
      })[0];
      if (!li || li.length === 0)
        return {};
      else
        return li
    },
    hasObjectById(list, id, idKey) {
      var ids = [];
      list.forEach(item => {
        ids.push(parseInt(idKey ? item[idKey] : item.ID))
      })
      return ids.includes(parseInt(id));
    }
  }
}

function numberFilter(value) {
  return String(value).replace(/[^0-9۰-۹]/g, '')
}


export default {
  isValidNC, sendRequest, numberFilter,
}
