export function SmsStatus(statusId) {
  switch (statusId) {
    case 1 :
      return 'در صف ارسال';
    case 2 :
      return 'زمان بندی شده';
    case 4 :
      return 'ارسال شده به مخابرات';
    case 5 :
      return 'ارسال شده به مخابرات';
    case 6 :
      return 'خطا در ارسال';
    case 10 :
      return 'رسیده به گیرنده';
    case 11 :
      return 'نرسیده به گیرنده';
    case 13 :
      return 'بازگشت هزینه';
    case 14 :
      return 'بلاک شده توسط کاربر';
    case 100 :
      return 'عدم اعتبار شناسه پیامک';
  }
}
