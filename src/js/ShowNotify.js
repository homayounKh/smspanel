import {Notify} from "quasar";

export function ShowNotify({
  message = '', color = 'primary', textColor = 'white'
              }) {
  Notify.create({
    message: message,
    color: color,
    textColor: textColor,
    timeout: 2000,
    position: 'top-right',
    progress: true,
  })
}
