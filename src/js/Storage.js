import {LocalStorage} from "quasar";

function set(key, value){
  LocalStorage.set(key, value)
}

function get(key){
  if(!LocalStorage.has(key))
    return ''

  return LocalStorage.getItem(key);
}

function remove(key){
  return LocalStorage.remove(key)
}

function has(key){
  return LocalStorage.has(key)
}

export default {
  set,
  get,
  remove,
  has,
}
