import {Cookies} from "quasar";

function set(key, value, ts = '') {
  if (!key) {
    throw "SET COOKIE ERROR : key is empty"
  }

  if (!ts)
    ts = 2592000;
  const date = new Date(Date.now() + (ts * 1000));

  if (typeof value === "object")
    value = JSON.stringify(value)

  value = btoa(unescape(encodeURIComponent(value)))

  console.log(key)
  console.log(value)

  Cookies.set(key, value, {expires: date})
}

function get(key) {
  if(!Cookies.has(key))
    return

  let value = Cookies.get(key);
  return decodeURIComponent(escape(window
    .atob(value)));
}

function has(key) {
  return Cookies.has(key);
}

function remove(key) {
  Cookies.remove(key);
  // document.cookie = key + '=' + ";expires=Thu, 01 Jan 1970 00:00:01 GMT ;path=/";
}


export default {set, get, has, remove}
