export const Numbers = {
  methods: {
    doVersion(n) {
      if (n == null) {
        n = "0";
        return n;
      }

      n = n.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".");
      return n;
    },
    En2Pr(n, comma = false) {
      if (n == null) {
        n = "۰";
        return n;
      }

      n = comma ? n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : n;
      n = n.toString();
      return n
        .replace(/1/g, "۱")
        .replace(/2/g, "۲")
        .replace(/3/g, "۳")
        .replace(/4/g, "۴")
        .replace(/5/g, "۵")
        .replace(/6/g, "۶")
        .replace(/7/g, "۷")
        .replace(/8/g, "۸")
        .replace(/9/g, "۹")
        .replace(/0/g, "۰");
    },
    Pr2En(n, comma = false) {
      if (n == null) {
        n = "0";
        return n;
      }

      n = comma ? n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : n;
      n = n.toString();
      return n
        .replace(/۱/g, "1")
        .replace(/۲/g, "2")
        .replace(/۳/g, "3")
        .replace(/۴/g, "4")
        .replace(/۵/g, "5")
        .replace(/۶/g, "6")
        .replace(/۷/g, "7")
        .replace(/۸/g, "8")
        .replace(/۹/g, "9")
        .replace(/۰/g, "0");
    },
    rmComma(num_str, toInt = false) {
      if (num_str.indexOf(",") !== -1) {
        num_str = num_str.replace(/,/g, "");
      }
      if (toInt) {
        num_str = this.Pr2En(num_str, false);
        return parseInt(num_str);
      }
      return num_str;
    },
    setComma(num_str){
      return num_str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }
};

export const PersianTools = {
  methods: {
    fixArabicString(text) {
      if (!text) return '';
      text = text.replace(/ک/g, 'ک').replace(/ی/g, 'ی').replace(/ى/g, 'ی');
      return text;
    }
  }
};

export const Validate = {
  methods: {
    iso7064Mod97_10(iban) {
      var remainder = iban,
        block;

      while (remainder.length > 2) {
        block = remainder.slice(0, 9);
        remainder = (parseInt(block, 10) % 97) + remainder.slice(block.length);
      }

      return parseInt(remainder, 10) % 97;
    },
    validateSheba(str) {
      var pattern = /IR[0-9]{24}/;

      if (str.length !== 26) {
        return false;
      }

      if (!pattern.test(str)) {
        return false;
      }

      var newStr = str.substr(4);
      var d1 = str.charCodeAt(0) - 65 + 10;
      var d2 = str.charCodeAt(1) - 65 + 10;
      newStr += d1.toString() + d2.toString() + str.substr(2, 2);

      var remainder = this.iso7064Mod97_10(newStr);
      if (remainder !== 1) {
        return false;
      }

      return true;
    },
    validateNationalCode(input) {
      if (!/^\d{10}$/.test(input)) return false;

      var check = +input[9];
      var sum = 0;
      var i;
      for (i = 0; i < 9; ++i) {
        sum += +input[i] * (10 - i);
      }
      sum %= 11;

      return (sum < 2 && check == sum) || (sum >= 2 && check + sum == 11);
    }
  }
};

import jmoment from "moment-jalaali";
import fa from "moment/src/locale/fa";
jmoment.locale("fa", fa);
jmoment.loadPersian();
export const ConvertTo = {
  methods: {
    toNormalDate(date) {
      var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = "0" + month;
      if (day.length < 2) day = "0" + day;

      return [year, month, day].join("-");
    },
    toJalali(input) {
      var JDate = require("jalaali-js");
      var _arr = input.split("-");
      console.log(_arr);
      return JDate.toJalaali(
        parseInt(_arr[0]),
        parseInt(_arr[1]),
        parseInt(_arr[2])
      );
    },
    toMiladi(input) {
      var JDate = require("jalaali-js");
      var _arr = input.split("/");
      return JDate.toGregorian(
        parseInt(_arr[0]),
        parseInt(_arr[1]),
        parseInt(_arr[2])
      );
    },
    toJalaliStr(input, itime = false, otime = false) {
      if (itime && !otime)
        return jmoment(input, "YYYY-MM-DD HH:mm:ss").format("jYYYY/jMM/jDD");
      else if (itime && otime)
        return jmoment(input, "YYYY-MM-DD HH:mm:ss").format(
          "jYYYY/jMM/jDD HH:mm:ss"
        );

      return jmoment(input, "YYYY-MM-DD").format("jYYYY/jMM/jDD");
    },
    toMiladiStr(input) {
      if(!input)
        return '';
      return jmoment(input, "jYYYY/jMM/jDD").format("YYYY-MM-DD");
    },

    // 00:00
    sec2minSec_pr(sec){
      return Numbers.methods.En2Pr(Math.floor(sec / 60)) + ":" +
             Numbers.methods.En2Pr(Math.floor(sec % 60))
    },
    sec2minSec_en(sec){
      return Math.floor(sec / 60) + ":" + Math.floor(sec % 60);
    },
  }
};
