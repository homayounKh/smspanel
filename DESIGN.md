## components:


### BoxShadow :
برای دسته بنده اطلاعات مرتبط در صفحات می توان از این component استفاده کرد
المان ها به عنوان child در بدنه component قرار می گیرند


### CustomInput :
برای دریافت ورودی های متنی از این component استفاده می شود
با تغییر prop "type" می توان نوع ورودی را مشخص کرد که شامل موارد زیر است :

**text** : دریافت ورودی ساده متنی

**date** : انتخاب تاریخ

**time** : تعیین زمان 

**select** : انتخاب از منوی کشویی


### CustomModal :
برای ایجاد modal در صفحات استفاده می شود

المان ها به عنوان child در بدنه component قرار می گیرند


### CustomTable :
برای ایجاد جدول استفاده می شود
با استفاده از سه متد زیر میتوان جدول را مقدارگذاری کرد :

setDataList : تعیین داده های جدول

setDataHeader : تعیین تایتل هدر و سایر اطلاعات مربوط به هر سلول

setBtns : مشخص کردن اطلاعات مربوط به دکمه ها


### TrackingTable :
جدول پیگیری و فرایند های مربوط به آن (حذف پیام ها و ...) در این component قرار دارد